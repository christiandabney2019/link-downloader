function startDownload() {
    var link = document.getElementById("linkInput").value;
    
    // Display the download message
    var downloadMessage = document.getElementById("downloadMessage");
    downloadMessage.style.display = "block";
    downloadMessage.innerHTML = "Downloading will start in 5 seconds...";
    
    // Simulate the download delay
    setTimeout(function() {
        // Redirect to the download link
        window.location.href = link;
    }, 5000);
}